//importando dependencias necessárias
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
//rota do arquivo para execução/leitura
ReactDOM.render(<App/>,document.getElementById('root'));
