//importando react native
import React, { Component } from 'react';
//importando arquivo api na raiz
import api from './api';

class App extends Component{
//criação da chamada json com nome de avatar_url
  state= {
    avatar_url: [],
  }
//tipo de requisição, no caso em branco
  async componentDidMount(){
    const response = await api.get('');
    
    
    console.log(response.data);

    this.setState({ avatar_url: response.data});
  }

  render(){
    //informando qual a chave para a coleta
    const { avatar_url } = this.state;
//estrutura que será retornada na pagina
    return(
      <div>
        <h1>Listar url avatar</h1>
        {console.log(avatar_url)}
        {avatar_url.map(avatar_url => (
          <li key={avatar_url.owner.id}>
            <h2>
              <strong>Login: </strong>
              {avatar_url.owner.login}
              </h2>
              			  <p> 
                {avatar_url.owner.type}
                {avatar_url.owner.avatar_url}
              </p>

          </li>
        ))}
      </div>
    );
  };
};

export default App;
