//integração do axios
import axios from 'axios';
import App from './App';

//chamada de url utilizando axios
const api = axios.create({
    baseURL: 'https://api.github.com/users/takenet/repos'
});

export default api;